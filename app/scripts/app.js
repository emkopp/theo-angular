'use strict';

/**
 * @ngdoc overview
 * @name yajsbApp
 * @description
 * # theoApp
 *
 * Main module of the application.
 */
var a = angular
  .module('theo', [
    'ui.router',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.grid',
    'ui.grid.edit',
    'angular-growl'
  ])
  .constant('ENDPOINT_URI','http://localhost/')
  .config(function ($stateProvider,$urlRouterProvider){
    $urlRouterProvider.otherwise('/main');


    $stateProvider
      .state('login', {
      url:'/info',
      templateUrl:'login/login.tmpl.html',
      controller:'LoginCtrl',
      controllerAs:'login'
    })
    .state('commands', {
      url:'/commands',
      templateUrl:'commands/commands.tmpl.html',
      controller:'CommandsCtrl',
      controllerAs:'commands',
    })
      .state('main', {
        url:'/main',
        templateUrl:'main/main.tmpl.html',
        controller:'MainCtrl',
        controllerAs:'main',
      })
})



a.config(['growlProvider', function (growlProvider) {
  growlProvider.globalTimeToLive(3000);
  growlProvider.onlyUniqueMessages(false);
}]);



