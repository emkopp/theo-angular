'use strict';

/**
 * @ngdoc function
 * @name yajsbApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yajsbApp
 */
var app = angular.module('theo')

var fnctLoginCtrl = function ($scope){
  $scope.awesomeThings = [
    'HTML5 Boilerplate',
    'AngularJS',
    'Karma'
  ];
  $scope.stuff = "Demo Information";
}

app.controller('LoginCtrl', fnctLoginCtrl );


