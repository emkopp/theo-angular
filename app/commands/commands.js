'use strict';

/**
 * @ngdoc function
 * @name yajsbApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yajsbApp
 */
var app = angular.module('theo')

var fnctCommandsCtrl;
fnctCommandsCtrl = function ($scope, CommandChatOp,MainDataOp,growl) {
  $scope.awesomeThings = [
    'HTML5 Boilerplate',
    'AngularJS',
    'Karma'
  ];
  var domain = theoConfig.urlBase;
  var webpath = theoConfig.HubPath;
  var urlBase = "http://" + domain + webpath + "/signalr";

  $scope.title = "Command Template";
  $scope.groups;
  $scope.data = {};
  $scope.chat = "";

  $scope.ChatFunction = function (message){
    $scope.chat = (new Date().toLocaleTimeString()) + ' | ' + message + '\n' + $scope.chat;
    console.log(message);
   setTimeout(function (){
     $scope.$apply();
   },1);
  }

  $scope.SetupConnection = function (url,hubname) {
    var connection = $.hubConnection(url, {useDefaultPath: false});
    var theoHub = connection.createHubProxy(hubname);
    theoHub.on('TheoTalkback', function (text) {
      $scope.ChatFunction(text);
    });
    connection.start()
      .done(function () {
        $scope.ChatFunction('Now connected, connection ID=' + connection.id);
        theoHub.invoke('JoinGroup',"MYGROUP");
      })
      .fail(function () {
        $scope.ChatFunction('Could not connect')
      });



  }

  $scope.DoPing = function (data) {

    if (data.pingurl == undefined || data.group == undefined){
      growl.error("Ping URL and Group Required");
      return;
    }

    var commandDTO = MainDataOp.GetCommandDTO();
    commandDTO.Action = "PingGroup";
    commandDTO.CallbackGroup = "TheoTalkback";
    commandDTO.Message = data.pingurl;
    commandDTO.CommandGroup = data.group;
    commandDTO.FromUser = "Eric Kopp";
    (function (){MainDataOp.doPing(commandDTO)})();
    console.log("test");
    $scope.ChatFunction( "Exectued " + commandDTO.Action + ' for ' + data.group);

  }

  $scope.SetupConnection(urlBase, "theoHub");

  $scope.GetGroupsTheo = function (){
    MainDataOp.GetGroupsTheo()
      .success(function (mains) {
        $scope.groups = mains;
      })
      .error(function (error) {
        $scope.status = error.message;
      })
  }

  $scope.GetGroupsTheo();
};

app.controller('CommandsCtrl', fnctCommandsCtrl );


