/**
 * Created by erickopp on 9/30/15.
 */

var app = angular.module('theo')

app.factory('CommandChatOp', ['$http', function ($http) {

  var CommandChatOp = {};
  CommandChatOp.Connections = [];

  CommandChatOp.SetupConnection = function (url,hubname, chatFunction) {

    connection = $.hubConnection(url, {useDefaultPath: false});
    theoHub = connection.createHubProxy(hubname);
    theoHub.on('TheoTalkback', function (text) {
      chatFunction( text);
    });
    connection.start()
      .done(function () {
        chatFunction('Now connected, connection ID=' + connection.id);
       })
      .fail(function () {
        chatFunction('Could not connect')
      });
    CommandChatOp[hubname] = connection;
  }
  return CommandChatOp;
}
])
