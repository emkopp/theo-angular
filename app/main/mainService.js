/**
 * Created by erickopp on 9/30/15.
 */


var app = angular.module('theo')

app.factory('MainDataOp', ['$http', function ($http) {

  var domain = theoConfig.urlBase;
  var webpath = theoConfig.HubPath;
  var urlBase = "http://" + domain + webpath  + "/api";
  var MainDataOp = {};

  MainDataOp.GetCommandDTO = function (){
    var commandDTO = {};
    commandDTO.Action = "";
    commandDTO.CallbackGroup = "";
    commandDTO.Message = "";
    commandDTO.FromUser = "";
    return commandDTO;
  };

  var httpConfig = {timeout:2000};



  MainDataOp.doPing = function (commandDTO) {
    return $http.put(urlBase + "/pinggroup", commandDTO);
  }

  MainDataOp.getClients = function () {
    return $http.get(urlBase + "/lookups/clients",httpConfig);
  }

  MainDataOp.getEnvironments = function () {
    return $http.get(urlBase + "/lookups/environments",httpConfig);
  }

  MainDataOp.getChannels = function () {
    return $http.get(urlBase + "/lookups/channels",httpConfig);
  }

  MainDataOp.getLayers = function () {
    return $http.get(urlBase + "/lookups/layers",httpConfig);
  }

  MainDataOp.getModules = function () {
    return $http.get(urlBase + "/lookups/modules",httpConfig);
  }

  MainDataOp.getPDF = function (data) {
    return $http.post(urlBase + "/platformdefinition", data);
  }

  MainDataOp.ReloadPDF = function (data) {
    return $http.post(urlBase + "/reloadplatformdefinition", data);
  }

  MainDataOp.updatePdf = function (data){
    return $http.post(urlBase + "/UpdatePdf",data);
  }


  MainDataOp.GetGroups = function () {
    return $http.get(urlBase + "/getgroups");
  }

  MainDataOp.GetGroupsTheo = function () {
    return $http.get(urlBase + "/getgroups/theo");
  }

  MainDataOp.MessageGroup = function (data) {
    return $http.post(urlBase + "/messagegroup", data);
  }


  return MainDataOp;

}


])
