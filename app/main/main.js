'use strict';

/**
 * @ngdoc function
 * @name yajsbApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yajsbApp
 */
var app = angular.module('theo')

var fnctMainCtrl;
fnctMainCtrl = function ($scope, MainDataOp,growl) {

var config = {};

  $scope.clients;
  $scope.layers;
  $scope.modules;
  $scope.channels;
  $scope.environments;
  $scope.pdf;
  $scope.myData;


  $scope.msg = {};
  $scope.gridOptions = {};

  $scope.gridOptions.columnDefs = [
    { name: 'key', displayName: "Reference Key", enableCellEdit: false, width: '50%' },
    { name: 'val', displayName: 'Value (Editable)', enableCellEdit:true, width: '50%' }];

  $scope.gridOptions.onRegisterApi = function(gridApi){
    //set gridApi on scope
    $scope.gridApi = gridApi;
    gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef){
      $scope.msg.lastCellEdited = 'edited row id:' + rowEntity.key + ' Column:' + rowEntity.val;
      $scope.$apply();

      var datatosend = {};
      datatosend.Client = $scope.data.client;
      datatosend.Layer = $scope.data.layer;
      datatosend.Module = $scope.data.module;
      datatosend.Channel = $scope.data.channel;
      datatosend.Environment = $scope.data.environment;
      datatosend.key = rowEntity.key;
      datatosend.val = rowEntity.val;
      $scope.UpdatePdf(datatosend);
      growl.success("In Update for Grid",config);
    });
  };

  $scope.UpdatePdf = function (data) {
    MainDataOp.updatePdf(data)
      .then(function successCallback (response) {
        $scope.gridOptions.data = response.data;
        growl.success("Updated PDF",config);
      },function errorCallback (response) {
        growl.error("Connection error",config);
        $scope.status = error.message;
      })
  }



  $scope.GetPDFClick = function (data) {
    MainDataOp.getPDF(data)
      .success(function (mains) {

        $scope.gridOptions.data = mains;
        growl.success("Loaded PDF",config);

      })
      .error(function (error) {
        growl.error(error.message,config);
        $scope.status = error.message;
      })
  }

  $scope.ReloadPDFClick = function (data) {
    MainDataOp.ReloadPDF(data)
      .success(function (mains) {
        $scope.gridOptions.data = mains;
        growl.success("Reloaded PDF",config);
      })
      .error(function (error) {
        growl.error(error.message,config);
        $scope.status = error.message;
      })
  }

  MainDataOp.getClients()
    .then(function successCallback (response) {
      $scope.clients = response.data;
      growl.success("Clients Loaded",config);
    },function errorCallback (response) {
      growl.error("Connection error",config);
      $scope.status = error.message;
    })


  MainDataOp.getEnvironments()
    .then(function successCallback (response) {
      $scope.environments = response.data;
      growl.success("Environments Loaded",config);
    },function errorCallback (response) {
      growl.error("Connection error",config);
      $scope.status = error.message;
    })


  MainDataOp.getLayers()
    .then(function successCallback (response) {
      $scope.layers = response.data;
      growl.success("Layers Loaded",config);
    },function errorCallback (response) {
      growl.error("Connection error",config);
      $scope.status = error.message;
    })
  MainDataOp.getModules()
    .then(function successCallback (response) {
      $scope.modules = response.data;
      growl.success("Modules Loaded",config);
    },function errorCallback (response) {
      growl.error("Connection error",config);
      $scope.status = error.message;
    })

  MainDataOp.getChannels()
    .then(function successCallback (response) {
      $scope.channels = response.data;
      growl.success("Channels Loaded",config);
    },function errorCallback (response) {
      growl.error("Connection error",config);
      $scope.status = error.message;
    })

};



app.controller('MainCtrl', fnctMainCtrl );


